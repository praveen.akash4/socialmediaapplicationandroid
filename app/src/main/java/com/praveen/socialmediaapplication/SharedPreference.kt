package com.praveen.socialmediaapplication

import android.content.Context
import android.content.SharedPreferences

object SharedPreference {

    private const val PREFS_NAME = "my_app_prefs"
    private const val ACCESS_TOKEN_KEY = "access_token"
    private const val LOGIN_STATUS = "login_status"

    private fun getPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
    }

    fun saveAccessToken(context: Context, accessToken: String) {
        val editor = getPreferences(context).edit()
        editor.putString(ACCESS_TOKEN_KEY, accessToken)
        editor.apply()
    }

    fun getAccessToken(context: Context): String? {
        return getPreferences(context).getString(ACCESS_TOKEN_KEY, null)
    }

    fun setLoginStatus(context: Context, loginStatus: Boolean) {
        val editor = getPreferences(context).edit()
        editor.putBoolean(LOGIN_STATUS, loginStatus)
        editor.apply()
    }

    fun isLoginStatus(context: Context): Boolean {
        return getPreferences(context).getBoolean(LOGIN_STATUS, false)
    }
}