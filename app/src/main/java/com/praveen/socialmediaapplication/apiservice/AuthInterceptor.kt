package com.praveen.socialmediaapplication.apiservice

import android.util.Log
import com.praveen.socialmediaapplication.SharedPreference
import com.praveen.socialmediaapplication.application.MyApp
import okhttp3.Interceptor
import okhttp3.Response

class AuthInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val token = SharedPreference.getAccessToken(MyApp.appContext)
        Log.d("AuthInterceptor ", "Token value ${token}")
        val requestBuilder = original.newBuilder()
            .header("Authorization", "Bearer $token")
        val request = requestBuilder.build()
        return chain.proceed(request)
    }
}