package com.praveen.socialmediaapplication.apiservice

import com.praveen.socialmediaapplication.data.Login
import com.praveen.socialmediaapplication.data.LoginResponse
import com.praveen.socialmediaapplication.data.User
import com.praveen.socialmediaapplication.data.UserResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface APIService {

    @POST("/user/authenticate")
    suspend fun login(@Body login: Login) : Response<LoginResponse>

    @POST("/user/new")
    suspend fun addNewUser(@Body user: User) : Response<UserResponse>

    @GET("/user/search")
    suspend fun searchUser(@Query("username") query: String) : Response<List<User>>

    @GET("/follow-requests/send")
    suspend fun sendFollowRequest(
        @Query("fromUsername") fromUserName: String,
        @Query("toUsername") toUserName: String
    ): Response<String>
}