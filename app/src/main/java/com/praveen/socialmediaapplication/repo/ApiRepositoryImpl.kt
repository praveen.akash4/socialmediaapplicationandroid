package com.praveen.socialmediaapplication.repo

import com.praveen.socialmediaapplication.ApiRespone
import com.praveen.socialmediaapplication.apiservice.APIService
import com.praveen.socialmediaapplication.data.Login
import com.praveen.socialmediaapplication.data.LoginResponse
import com.praveen.socialmediaapplication.data.User
import com.praveen.socialmediaapplication.data.UserResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import javax.inject.Named

class ApiRepositoryImpl @Inject constructor(
    @Named("noAuth") private val apiServiceNoAuth: APIService,
    @Named("auth") private val apiServiceAuth: APIService
) : ApiRepository {
    override suspend fun addUser(user: User): Flow<ApiRespone<UserResponse>> {
        return flow {
            try {
                val response = apiServiceNoAuth.addNewUser(user)
                if (response != null && response.isSuccessful) {
                    emit(ApiRespone.Success(response.body()!!))
                } else {
                    emit(ApiRespone.Error(response.code(), response.errorBody()?.string()))
                }
            } catch (e: Exception) {
                emit(ApiRespone.Error(110, e.localizedMessage))
            }
        }
    }

    override suspend fun loginApi(login: Login): Flow<ApiRespone<LoginResponse>> {
        return flow {
            try {
                val response = apiServiceNoAuth.login(login)
                if (response != null && response.isSuccessful) {
                    emit(ApiRespone.Success(response.body()!!))
                } else {
                    emit(ApiRespone.Error(response.code(), response.errorBody()?.string()))
                }
            } catch (e: Exception) {
                emit(ApiRespone.Error(110, e.localizedMessage))
            }
        }
    }

    override suspend fun getUsers(userString: String): Flow<ApiRespone<List<User>>> {
        return flow {
            try {
                val response = apiServiceAuth.searchUser(userString)
                if (response != null && response.isSuccessful) {
                    emit(ApiRespone.Success(response.body()!!))
                } else {
                    emit(ApiRespone.Error(response.code(), response.errorBody()?.string()))
                }
            } catch (e: Exception) {
                emit(ApiRespone.Error(110, e.localizedMessage))
            }
        }
    }

    override suspend fun sendFollowRequest(fromUsername: String, toUsername: String) : Flow<ApiRespone<String>>{
        return flow {
            try {
                val response = apiServiceAuth.sendFollowRequest(fromUsername, toUsername)
                if (response != null && response.isSuccessful) {
                    emit(ApiRespone.Success(response.body()!!))
                } else {
                    emit(ApiRespone.Error(response.code(), response.errorBody()?.string()))
                }
            } catch (e: Exception) {
                emit(ApiRespone.Error(110, e.localizedMessage))
            }
        }
    }
}