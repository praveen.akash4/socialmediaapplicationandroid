package com.praveen.socialmediaapplication.repo

import com.praveen.socialmediaapplication.ApiRespone
import com.praveen.socialmediaapplication.data.Login
import com.praveen.socialmediaapplication.data.LoginResponse
import com.praveen.socialmediaapplication.data.User
import com.praveen.socialmediaapplication.data.UserResponse
import kotlinx.coroutines.flow.Flow
import retrofit2.Response

interface ApiRepository {

    suspend fun addUser(user: User) : Flow<ApiRespone<UserResponse>>?

    suspend fun loginApi(login: Login) : Flow<ApiRespone<LoginResponse>>

    suspend fun getUsers(user: String) : Flow<ApiRespone<List<User>>>

    suspend fun sendFollowRequest(fromUsername: String, toUsername: String) : Flow<ApiRespone<String>>
}