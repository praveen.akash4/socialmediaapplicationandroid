package com.praveen.socialmediaapplication.helper

import com.praveen.socialmediaapplication.apiservice.APIService
import com.praveen.socialmediaapplication.apiservice.AuthInterceptor
import com.praveen.socialmediaapplication.repo.ApiRepository
import com.praveen.socialmediaapplication.repo.ApiRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named


@Module
@InstallIn(SingletonComponent::class)
object RetrofitModule {
    private const val BASE_URL = "http://10.0.2.2:8080"

    @Provides
    fun getRepository(
        @Named("noAuth") apiServiceNoAuth: APIService,
        @Named("auth") apiServiceAuth: APIService
    ): ApiRepository {
        return ApiRepositoryImpl(apiServiceNoAuth, apiServiceAuth)
    }

    // Create a logging interceptor
    private val loggingInterceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

    // Create an OkHttpClient and add the interceptor
    private fun getBuilder(): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor).build()
    }

    @Provides
    @Named("noAuth")
    fun getRetrofitDependency(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL) // Replace with your base URL
            .addConverterFactory(GsonConverterFactory.create())
            .client(getBuilder())
            .build()
    }

    @Provides
    fun provideAuthInterceptor(): AuthInterceptor {
        return AuthInterceptor()
    }

    @Provides
    @Named("auth")
    fun provideRetrofitWithAuth(
        authInterceptor: AuthInterceptor?
    ): Retrofit {
        val client: OkHttpClient = OkHttpClient.Builder()
            .addInterceptor(authInterceptor!!)
            .build()

        return Retrofit.Builder()
            .baseUrl(BASE_URL) // Replace with your base URL
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
    }

    @Provides
    @Named("noAuth")
    fun provideApiServiceWithoutAuth(@Named("noAuth") retrofit: Retrofit): APIService {
        return retrofit.create(APIService::class.java)
    }

    @Provides
    @Named("auth")
    fun provideApiServiceWithAuth(@Named("auth") retrofit: Retrofit): APIService {
        return retrofit.create(APIService::class.java)
    }
}