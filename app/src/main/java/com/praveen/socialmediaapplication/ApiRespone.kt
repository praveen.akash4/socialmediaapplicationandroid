package com.praveen.socialmediaapplication

sealed class ApiRespone<out T> {

    data class Success<out T>(val data: T) : ApiRespone<T>()
    data class Error(val statusCode: Int, val message: String?) : ApiRespone<Nothing>()
    object Loading : ApiRespone<Nothing>()
}
