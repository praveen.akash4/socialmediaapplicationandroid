package com.praveen.socialmediaapplication.data

data class LoginResponse(
    val accessToken: String,
    val message: String,
    val statusCode: Int,
    val refreshToken: String,
    val userName: String
)