package com.praveen.socialmediaapplication.data

data class FeedItem(
    val id: String,
    val userName: String,
    val userImage: Int,
    val title: String,
    val description: String,
    val imageUrl: String,
    val likes: Int,
    val comments: Int
)
