package com.praveen.socialmediaapplication.data

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable

@Serializable
data class User(
    @SerializedName("userId")
    val userId: Int,

    @SerializedName("actualUserName")
    val actualUserName: String?, // Use camelCase for better readability

    @SerializedName("bio")
    val bio: String?,

    @SerializedName("createdAt")
    var createdAt: String?,

    @SerializedName("password")
    var password: String?,

    @SerializedName("profilePicture")
    val profilePicture: String?,

    @SerializedName("roles")
    val roles: String?,

    @SerializedName("username")
    val userName: String?,

    @SerializedName("email")
    val email: String?
)
