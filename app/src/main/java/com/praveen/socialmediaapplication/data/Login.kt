package com.praveen.socialmediaapplication.data

data class Login(
    val username: String,
    val password: String
)
