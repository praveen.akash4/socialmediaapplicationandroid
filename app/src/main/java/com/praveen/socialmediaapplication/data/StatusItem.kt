package com.praveen.socialmediaapplication.data

data class StatusItem(
    val id: String,
    val userName: String,
    val userImage: Int,
    val title: String,
    val description: String,
    val imageUrl: String,
)
