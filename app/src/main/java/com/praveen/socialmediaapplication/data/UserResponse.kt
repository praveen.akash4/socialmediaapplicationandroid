package com.praveen.socialmediaapplication.data

data class UserResponse (
    val message: String,
    val statusCode: Int
)
