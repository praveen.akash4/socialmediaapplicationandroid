package com.praveen.socialmediaapplication.bottomnavigation

sealed class Screens(val route: String) {
    object Home : Screens("home_route")
    object Search : Screens("search_route")
    object Profile : Screens("profile_route")
    object AddPost : Screens("add_post")
    object SignUp : Screens("signup_route")
    object ForgotPassword : Screens("forgot_password_route")
    object Login : Screens("login_route")
    object AppScafold : Screens("app_scafold")
    object PasswordScreen : Screens("enter_password")
    object SearchUserProfileScreen: Screens("SUPS")
}