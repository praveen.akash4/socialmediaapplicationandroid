package com.praveen.socialmediaapplication.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.praveen.socialmediaapplication.R
import com.praveen.socialmediaapplication.bottomnavigation.Screens
import com.praveen.socialmediaapplication.data.User
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.net.URLEncoder
import java.nio.charset.StandardCharsets

@Composable
fun SignUpScreen(navController: NavController) {
    var emailText = remember { mutableStateOf("") }
    val fullName = remember { mutableStateOf("") }
    val mobileNumber = remember { mutableStateOf("") }
    var isError by remember { mutableStateOf(false) }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            painter = painterResource(id = R.drawable.blackcat),
            contentDescription = "Black Cat",
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(0.35f)
        )

        Spacer(modifier = Modifier.height(5.dp))
        Text(
            text = "Sign Up",
            fontSize = 24.sp,
            fontWeight = FontWeight.Bold,
            modifier = Modifier.align(Alignment.Start)
        )
        Spacer(modifier = Modifier.height(20.dp))

        OutlinedTextField(
            value = emailText.value,
            onValueChange = { emailText.value = it },
            label = { Text(text = "email ID") },
            leadingIcon = {
                Icon(
                    painter = painterResource(id = R.drawable.lockat), // Update with your icon resource
                    contentDescription = "Email Icon"
                )
            },
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(modifier = Modifier.height(16.dp))

        OutlinedTextField(
            value = emailText.value,
            onValueChange = { emailText.value = it },
            label = { Text(text = "actual user name") },
            leadingIcon = {
                Icon(
                    painter = painterResource(id = R.drawable.lockperson), // Update with your icon resource
                    contentDescription = "Email Icon"
                )
            },
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(modifier = Modifier.height(16.dp))

        OutlinedTextField(
            value = fullName.value,
            onValueChange = {
                fullName.value = it

                },
            label = { Text(text = "Suggestion name / Unique Name") },
            isError = isError,
            leadingIcon = {
                Icon(
                    painter = painterResource(id = R.drawable.lockperson), // Update with your icon resource
                    contentDescription = "Email Icon"
                )
            },
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(modifier = Modifier.height(16.dp))

        OutlinedTextField(
            value = mobileNumber.value,
            onValueChange = { mobileNumber.value = it },
            label = { Text(text = "mobile") },
            leadingIcon = {
                Icon(
                    painter = painterResource(id = R.drawable.lockphone), // Update with your icon resource
                    contentDescription = "Email Icon"
                )
            },
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(modifier = Modifier.height(20.dp))

        Text(text = "Join our coven and accept our Terms & Conditions and Privacy Policy. Don't be afraid, we don't bite!")

        Spacer(modifier = Modifier.height(40.dp))

        Button(
            onClick = {
                var user = User(1, fullName.value,"" , null, null,"","ADMIN",null,emailText.value)
                val userJson = Json.encodeToString(user)
                navController.navigate("enter_password/$userJson")
            },
            colors = ButtonDefaults.buttonColors(Color.Green),
            modifier = Modifier
                .fillMaxWidth()
                .height(48.dp)
        ) {
            Text(text = "Continue", color = Color.White)
        }

        Spacer(modifier = Modifier.height(20.dp))

        val annotatedText = remember {
            AnnotatedString.Builder("Are you a familiar spirit ? again and join our Halloween party!")
                .apply {
                    addStringAnnotation(
                        tag = "JOIN_COVEN",
                        annotation = "Join Our Coven!",
                        start = 24,
                        end = 39
                    )
                }.toAnnotatedString()
        }
        ClickableText(
            text = annotatedText,
            onClick = { offset ->
                annotatedText.getStringAnnotations(
                    tag = "JOIN_COVEN",
                    start = offset,
                    end = offset
                )
                    .firstOrNull()?.let {
                        // Handle the click event
                    }
            },
            style = LocalTextStyle.current.copy(
                color = Color.Gray,
                fontSize = 14.sp,
                textAlign = TextAlign.Center
            )
        )
    }
}

@Preview(showBackground = true)
@Composable
fun SignUpScreenPreview() {
    SignUpScreen(rememberNavController())
}