package com.praveen.socialmediaapplication.composables

import SearchBar1
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.praveen.socialmediaapplication.SharedPreference
import com.praveen.socialmediaapplication.bottomnavigation.BottomNavigationBar
import com.praveen.socialmediaapplication.bottomnavigation.Screens
import com.praveen.socialmediaapplication.data.User
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json

@Composable
fun AppNavigation() {
    // Initialize the top-level NavController
    var navController = rememberNavController()
    var startDestination = Screens.Login.route
    if (SharedPreference.isLoginStatus(LocalContext.current)) {
        startDestination = Screens.AppScafold.route
    }

    NavHost(navController = navController, startDestination = startDestination) {
        composable(Screens.Login.route) {
            LoginScreen(navController)
        }
        composable(Screens.SignUp.route) {
            SignUpScreen(navController)
        }
        composable("enter_password/{userJson}") { backStackEntry ->
            val userJson = backStackEntry.arguments?.getString("userJson")
            val user = userJson?.let { Json.decodeFromString<User>(it) }
            if (user != null) {
                PassWordScreen(user, navController)
            }
        }
        composable(Screens.ForgotPassword.route) {
            ForgotPasswordScreen(navController)
        }
        composable(Screens.AppScafold.route) {
            HomeScreen {
                navController.navigate(Screens.Login.route)
            }
        }
    }
}

@Composable
fun HomeScreen(logout: () -> Unit) {
    val navController = rememberNavController()

    Scaffold(
        bottomBar = { BottomNavigationBar(navController) },
    ) { innerPadding ->
        NavHost(
            navController = navController,
            startDestination = Screens.Home.route,
            modifier = Modifier.padding(innerPadding)
        ) {
            composable(Screens.Home.route) { FeedScreen(navController) }
            composable(Screens.Search.route) { SearchBar1(navController) }
            composable(Screens.Profile.route) { ProfileScreen(navController) }
            composable("${Screens.SearchUserProfileScreen.route}/{userJson}") { navBackStackEntry ->
                val userJson = navBackStackEntry.arguments?.getString("userJson")
                val user = userJson?.let { Json.decodeFromString<User>(it) }
                if (user != null) {
                    SearchUserProfileScreen(user, navController)
                }
            }
            // composable(Screens.AddPost.route) { AddPostScreen(navController) }
            // logout()
        }
    }
}
