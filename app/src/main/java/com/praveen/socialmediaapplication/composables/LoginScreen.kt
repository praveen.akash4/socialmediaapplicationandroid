package com.praveen.socialmediaapplication.composables

import android.util.Log
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.praveen.socialmediaapplication.R
import com.praveen.socialmediaapplication.bottomnavigation.Screens
import com.praveen.socialmediaapplication.viewmodel.AuthViewModel
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.praveen.socialmediaapplication.ApiRespone
import com.praveen.socialmediaapplication.SharedPreference
import com.praveen.socialmediaapplication.data.Login

private val TAG: String? = "Login Screen"

@Composable
fun LoginScreen(navController: NavController, authViewModel: AuthViewModel = hiltViewModel()) {

    val context = LocalContext.current
    val user by authViewModel.user.collectAsState()
    val loginState by authViewModel.loginResponse.observeAsState()

    var userName by remember { mutableStateOf("") }
    var password by remember { mutableStateOf("") }

    val googleSignInClient: GoogleSignInClient by remember {
        mutableStateOf(
            GoogleSignIn.getClient(
                context,
                GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(context.getString(R.string.default_web_client_id))
                    .requestEmail()
                    .build()
            )
        )
    }

    val launcher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.StartActivityForResult()
    ) { result ->
        val task = GoogleSignIn.getSignedInAccountFromIntent(result.data)
        val account = task.getResult(ApiException::class.java)
        account?.idToken?.let { idToken -> authViewModel.signInWithGoogle(idToken) }
    }

    if (user != null) {
        navController.popBackStack()
        navController.navigate(Screens.AppScafold.route) {
            popUpTo(navController.graph.startDestinationId)
            launchSingleTop = true
        }
    } else {

        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Spacer(modifier = Modifier.height(16.dp))
            Image(
                painter = painterResource(id = R.drawable.sweet_franky), // Update with your image resource
                contentDescription = "Login Image",
                modifier = Modifier
                    .fillMaxWidth()
                    .fillMaxHeight(0.35f)
            )
            Spacer(modifier = Modifier.height(16.dp))
            Text(
                text = "Login",
                fontSize = 24.sp,
                fontWeight = FontWeight.Bold,
                modifier = Modifier.align(Alignment.Start)
            )
            Spacer(modifier = Modifier.height(16.dp))
            OutlinedTextField(
                value = userName,
                onValueChange = { userName = it },
                label = { Text(text = "email ID") },
                leadingIcon = {
                    Icon(
                        painter = painterResource(id = R.drawable.lockat), // Update with your icon resource
                        contentDescription = "Email Icon"
                    )
                },
                modifier = Modifier.fillMaxWidth()
            )
            Spacer(modifier = Modifier.height(16.dp))
            OutlinedTextField(
                value = password,
                onValueChange = { password = it },
                label = { Text(text = "Password") },
                leadingIcon = {
                    Icon(
                        painter = painterResource(id = R.drawable.lock), // Update with your icon resource
                        contentDescription = "Password Icon"
                    )
                },
                modifier = Modifier.fillMaxWidth()
            )
            Spacer(modifier = Modifier.height(8.dp))
            Text(
                text = "Forgot Password?",
                color = Color(0xFF66BB6A),
                modifier = Modifier.align(Alignment.End)
            )
            Spacer(modifier = Modifier.height(16.dp))

            when(val response = loginState) {
                is ApiRespone.Error -> {
                    Log.d(TAG, response.message!!)
                    Toast.makeText(
                        LocalContext.current,
                        "Error: ${response.message} Status Code: ${response.statusCode}",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                ApiRespone.Loading -> {
                     IndeterminateCircularProgress()
                }
                is ApiRespone.Success -> {
                    SharedPreference.setLoginStatus(LocalContext.current, true)
                    Toast.makeText(LocalContext.current, response.data.message, Toast.LENGTH_SHORT)
                        .show()
                    SharedPreference.saveAccessToken(context, response.data.accessToken)
                    navController.navigate(Screens.AppScafold.route)
                }
                null -> {
                    Log.d(TAG, "Null block executed")
                }
            }

            Button(
                onClick = { authViewModel.login(Login(userName, password)) },
                colors = ButtonDefaults.buttonColors(Color.Green),
                modifier = Modifier
                    .fillMaxWidth()
                    .height(48.dp)
            ) {
                Text(text = "Continue", color = Color.White)
            }
            Spacer(modifier = Modifier.height(16.dp))
            Text(
                text = "OR",
                textAlign = TextAlign.Center,
                modifier = Modifier.fillMaxWidth()
            )
            Spacer(modifier = Modifier.height(16.dp))

            OutlinedButton(
                onClick = {
                    val signInIntent = googleSignInClient.signInIntent
                    launcher.launch(signInIntent)
                },
                modifier = Modifier
                    .fillMaxWidth()
                    .height(48.dp)
            ) {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.Start
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.google), // Update with your Google icon resource
                        contentDescription = "Google Icon",
                        modifier = Modifier.size(24.dp)
                    )
                    Spacer(modifier = Modifier.width(8.dp))
                    Text(text = "Login With Google")
                }
            }
            Spacer(modifier = Modifier.height(16.dp))
            val annotatedText = remember {
                AnnotatedString.Builder("Haven't we seen you before? Join Our Coven!").apply {
                    addStringAnnotation(
                        tag = "JOIN_COVEN",
                        annotation = "Join Our Coven!",
                        start = 24,
                        end = 39
                    )
                }.toAnnotatedString()
            }
            ClickableText(
                text = annotatedText,
                onClick = { offset ->
                    annotatedText.getStringAnnotations(
                        tag = "JOIN_COVEN",
                        start = offset,
                        end = offset
                    )
                        .firstOrNull()?.let {
                            navController.navigate(Screens.SignUp.route)
                        }
                },
                style = LocalTextStyle.current.copy(
                    color = Color.Gray,
                    fontSize = 14.sp
                )
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun LoginScreenPreview() {
    LoginScreen(rememberNavController())
}


