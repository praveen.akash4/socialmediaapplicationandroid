package com.praveen.socialmediaapplication.composables

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.google.firebase.auth.FirebaseAuth

@Composable
fun SettingAndActivity(navController: NavController/*, firebaseUser: FirebaseUser?*/) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Row(modifier = Modifier.fillMaxWidth()) {
            Icon(imageVector = Icons.AutoMirrored.Filled.ArrowBack,
                contentDescription = "Back Button",
                modifier = Modifier
                    .size(24.dp)
                    .clickable { navController.popBackStack() })

            Spacer(modifier = Modifier.width(30.dp))

            Text(text = "Setting and Activity")
        }
        SettingSearchBar()
    }
}

@Composable
fun SettingSearchBar(/*firebaseUser: FirebaseUser?*/) {

    Spacer(modifier = Modifier.height(15.dp))
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        horizontalAlignment = Alignment.Start, verticalArrangement = Arrangement.Top) {
        Spacer(modifier = Modifier.width(10.dp))
        Text(text = "Add Account", modifier = Modifier)
        Spacer(modifier = Modifier.height(15.dp))
        Text(text = "Log out", color = Color.Red , modifier = Modifier.clickable {
             FirebaseAuth.getInstance().signOut()
        })
    }
}

@Preview(showBackground = true)
@Composable
fun SettingAndActivityPreview() {
    SettingAndActivity(navController = rememberNavController())
}
