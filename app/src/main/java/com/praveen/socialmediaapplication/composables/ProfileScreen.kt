package com.praveen.socialmediaapplication.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import coil.compose.rememberAsyncImagePainter
import com.praveen.socialmediaapplication.bottomnavigation.Screens
import com.praveen.socialmediaapplication.R

@Composable
fun ProfileScreen(navController: NavController) {

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Spacer(modifier = Modifier.height(16.dp))
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Icon(imageVector = Icons.Default.ArrowBack,
                contentDescription = "Back Button",
                modifier = Modifier
                    .size(24.dp)
                    .clickable { navController.popBackStack() })
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = "Praveen Akash", fontWeight = FontWeight.Bold, fontSize = 20.sp
                )
                if (true) {
                    Spacer(modifier = Modifier.width(4.dp))
                    Icon(
                        painter = painterResource(id = R.drawable.verfied_badge_small), // Ensure this drawable resource exists
                        contentDescription = "Verified",
                        tint = Color.Blue,
                        modifier = Modifier.size(16.dp)
                    )
                }
            }
            Icon(imageVector = Icons.Default.MoreVert,
                contentDescription = "Menu",
                modifier = Modifier
                    .size(24.dp)
                    .clickable { navController.navigate(Screens.Profile.route) })
        }

        Spacer(modifier = Modifier.height(10.dp))

        ProfileStats()

        Text(
            text = "Praveen akash", modifier = Modifier
                .align(Alignment.Start)
                .padding(20.dp, 0.dp)
        )
        Spacer(modifier = Modifier.height(5.dp))
        ProfileButtons()
        Spacer(modifier = Modifier.height(10.dp))
        ProfileContent()
    }
}

@Preview(showBackground = true)
@Composable
fun ProfileScreenPreview() {
    ProfileScreen(rememberNavController())
}

@Composable
fun ProfileContent() {
    LazyVerticalGrid(
        columns = GridCells.Fixed(3),
        contentPadding = PaddingValues(4.dp),
        modifier = Modifier.fillMaxSize()
    ) {
        items(10) {
            Image(
                painter = rememberAsyncImagePainter("https://your-image-url.com"),
                contentDescription = "Post Image",
                modifier = Modifier
                    .size(100.dp)
                    .padding(4.dp)
            )
        }
    }
}

@Composable
fun ProfileButtons() {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(5.dp),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Button(
            onClick = { /* Edit profile button clicked */ },
            colors = ButtonDefaults.buttonColors(Color.LightGray),
            shape = RoundedCornerShape(8.dp)
        ) {
            Text("Edit profile", color = Color.Black)
        }
        Button(
            shape = RoundedCornerShape(8.dp),
            onClick = { /* Share profile button clicked */ },
            colors = ButtonDefaults.buttonColors(Color.LightGray)
        ) {
            Text("Share profile", color = Color.Black)
        }
        Button(
            shape = RoundedCornerShape(8.dp),
            onClick = { /* Email button clicked */ },
            colors = ButtonDefaults.buttonColors(Color.LightGray)
        ) {
            Text("Email", color = Color.Black)
        }
    }
}

@Composable
fun ProfileStats() {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        // Profile Image
        Image(
            painter = painterResource(id = R.drawable.ic_launcher_background), // Ensure this drawable resource exists
            contentDescription = "Profile Picture",
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .size(80.dp)
                .clip(CircleShape)
        )

        Spacer(modifier = Modifier.width(16.dp))

        // Stats Column
        Column(
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = "151", fontWeight = FontWeight.Bold, fontSize = 18.sp
            )
            Text(text = "posts")
        }

        Column(
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = "112K", fontWeight = FontWeight.Bold, fontSize = 18.sp
            )
            Text(text = "followers")
        }

        Column(
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = "162", fontWeight = FontWeight.Bold, fontSize = 18.sp
            )
            Text(text = "following")
        }
    }
}