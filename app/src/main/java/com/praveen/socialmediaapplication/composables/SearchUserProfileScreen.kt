package com.praveen.socialmediaapplication.composables

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.praveen.socialmediaapplication.R
import com.praveen.socialmediaapplication.bottomnavigation.Screens
import com.praveen.socialmediaapplication.data.User
import com.praveen.socialmediaapplication.viewmodel.FollowRequestViewModel

@Composable
fun SearchUserProfileScreen(user: User, navController: NavController) {

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Spacer(modifier = Modifier.height(16.dp))
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Icon(imageVector = Icons.Default.ArrowBack,
                contentDescription = "Back Button",
                modifier = Modifier
                    .size(24.dp)
                    .clickable { navController.popBackStack() })
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = user.userName ?: "Instagram User",
                    fontWeight = FontWeight.Bold,
                    fontSize = 20.sp
                )
                if (true) {
                    Spacer(modifier = Modifier.width(4.dp))
                    Icon(
                        painter = painterResource(id = R.drawable.verfied_badge_small), // Ensure this drawable resource exists
                        contentDescription = "Verified",
                        tint = Color.Blue,
                        modifier = Modifier.size(16.dp)
                    )
                }
            }
            Icon(imageVector = Icons.Default.MoreVert,
                contentDescription = "Menu",
                modifier = Modifier
                    .size(24.dp)
                    .clickable { navController.navigate(Screens.Profile.route) })
        }

        Spacer(modifier = Modifier.height(10.dp))

        ProfileStats()

        Text(
            text = user.actualUserName?: "Instagram User", modifier = Modifier
                .align(Alignment.Start)
                .padding(20.dp, 0.dp)
        )
        Spacer(modifier = Modifier.height(10.dp))

        val viewModel: FollowRequestViewModel = hiltViewModel()
        val userResponse by viewModel.followStatus.observeAsState()

        Button(
            onClick = {
               // viewModel.sendFollowRequest(, " ")
            },
            modifier = Modifier.fillMaxWidth(),
            colors = ButtonDefaults.buttonColors(containerColor = Color(0xFF3797EF)),
            shape = RoundedCornerShape(10.dp)
        ) {
            Text(text = "Follow", color = Color.White)
        }
    }
}

@Composable
@Preview(showBackground = true)
fun SearchUserProfileScreenPreview() {
    SearchUserProfileScreen(User(1, "Praveen Kumar", "", "123456",
        "ADMIN","","ADMIN","Praveen","praveen.akash4@gmail.com"),rememberNavController())

}

