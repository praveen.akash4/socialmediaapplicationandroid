import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.praveen.socialmediaapplication.ApiRespone
import com.praveen.socialmediaapplication.R
import com.praveen.socialmediaapplication.bottomnavigation.Screens
import com.praveen.socialmediaapplication.composables.IndeterminateCircularProgress
import com.praveen.socialmediaapplication.data.User
import com.praveen.socialmediaapplication.viewmodel.SearchBarViewModel
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

private val TAG: String? = "Search Bar Screen"

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SearchBar1(navController: NavController) {

    val viewModel: SearchBarViewModel = hiltViewModel()

    var text by remember { mutableStateOf("") }
    val userState by viewModel.userItems.observeAsState()
    var usersList by remember { mutableStateOf(emptyList<User>()) }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Spacer(modifier = Modifier.height(10.dp))
        Surface(
            color = Color(0xFFD3D3D3), // Set the background color
            shape = RoundedCornerShape(24.dp),
            modifier = Modifier
                .fillMaxWidth()
        ) {
            OutlinedTextField(
                leadingIcon = {
                    IconButton(onClick = { /* Do something on click */ }) {
                        Image(imageVector = Icons.Default.Search, contentDescription = "Hello")
                    }
                },
                value = text,
                onValueChange = {
                    text = it
                    Log.d("SearchBarScreen", "Search Data $it")
                    viewModel.fetchData(it)
                },
                modifier = Modifier.fillMaxWidth(),
                textStyle = MaterialTheme.typography.bodyLarge.copy(color = Color.Black),
                singleLine = true,
                shape = RoundedCornerShape(24.dp),
                placeholder = {
                    Text(
                        text = "Search",
                        style = MaterialTheme.typography.bodyLarge.copy(color = Color.Gray)
                    )
                },
                colors = androidx.compose.material3.TextFieldDefaults.outlinedTextFieldColors(
                    cursorColor = Color.Black,
                    focusedBorderColor = Color.Transparent,
                    unfocusedBorderColor = Color.Transparent,
                )
            )

            when (val response = userState) {
                is ApiRespone.Error -> {
                    Log.d(TAG, "PassWordScreen: ${response.message}")
                    Toast.makeText(
                        LocalContext.current,
                        "Error: ${response.message} Status Code: ${response.statusCode}",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                ApiRespone.Loading -> {
                    IndeterminateCircularProgress()
                }

                is ApiRespone.Success -> {
                    Log.d(TAG, "Success Response API: ${response.data}")
                    usersList = response.data
                }

                null -> {
                    Log.d(TAG, "SearchBar Screen null tag")
                }
            }
        }

        SearchResults(results = usersList, navController)
    }
}

@Composable
fun SearchResults(results: List<User>, navController: NavController) {
    if (results.isEmpty()) {
        Text("No results found")
    } else {
        LazyColumn {
            items(results) { item ->
                Spacer(modifier = Modifier.height(16.dp))
                ProfileView(item, navController)
            }
        }
    }
}

@Composable
fun ProfileView(user: User, navController: NavController) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(3.dp)
            .clickable {
                val userJson = Json.encodeToString(user)
                navController.navigate("${Screens.SearchUserProfileScreen.route}/$userJson")
            },
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        // Profile Image
        Image(
            painter = painterResource(id = R.drawable.ic_launcher_background), // Ensure this drawable resource exists
            contentDescription = "Profile Picture",
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .size(60.dp)
                .clip(CircleShape)
        )

        Spacer(modifier = Modifier.width(16.dp))

        // Stats Column
        Column(
            modifier = Modifier.fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                modifier = Modifier.fillMaxWidth(),
                textAlign = TextAlign.Start,
                text = user.userName!!, fontWeight = FontWeight.Bold, fontSize = 18.sp
            )
            Text(
                modifier = Modifier.fillMaxWidth(),
                textAlign = TextAlign.Start,
                text = user.actualUserName!!
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun SearchBarPreview() {
    ProfileView(
        User(
            1,
            "Akash",
            "Hello",
            "",
            "12345678",
            "Hello",
            "ADMIN",
            "Praveen Akash",
            "praveen.akash4@gmail.com"
        ), rememberNavController()
    )
}

