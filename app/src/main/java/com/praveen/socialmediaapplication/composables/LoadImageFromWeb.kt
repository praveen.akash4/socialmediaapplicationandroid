package com.praveen.socialmediaapplication.composables

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.praveen.socialmediaapplication.data.StatusItem

@Composable
fun LoadImageFromWeb(statusItem: StatusItem) {
    Column(
        modifier = Modifier
            .wrapContentWidth()
            .padding(5.dp, 0.dp, 10.dp, 0.dp),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        AsyncImage(
            model = statusItem.imageUrl,
            contentDescription = "Sample Image",
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .size(80.dp)
                .clip(CircleShape)
        )

        Spacer(modifier = Modifier.height(5.dp))

        Text(text = statusItem.userName)
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewLoadImageFromWeb() {
    //LoadImageFromWeb()
}
