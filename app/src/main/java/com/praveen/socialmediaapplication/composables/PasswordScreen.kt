package com.praveen.socialmediaapplication.composables

import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.praveen.socialmediaapplication.ApiRespone
import com.praveen.socialmediaapplication.R
import com.praveen.socialmediaapplication.application.MyApp
import com.praveen.socialmediaapplication.bottomnavigation.Screens
import com.praveen.socialmediaapplication.data.User
import com.praveen.socialmediaapplication.viewmodel.UserViewModel

private val TAG: String? = "Password Screen"

@Composable
fun PassWordScreen(user: User, navController: NavController) {

    var passwordStr by remember { mutableStateOf(" ") }
    var passwordStrConf by remember { mutableStateOf(" ") }
    var passwordVisible by remember { mutableStateOf(false) }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            painter = painterResource(id = R.drawable.blackcat),
            contentDescription = "Black Cat",
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(0.35f)
        )

        Spacer(modifier = Modifier.height(5.dp))
        Text(
            text = "Sign Up",
            fontSize = 24.sp,
            fontWeight = FontWeight.Bold,
            modifier = Modifier.align(Alignment.Start)
        )
        Spacer(modifier = Modifier.height(20.dp))

        OutlinedTextField(
            value = passwordStr,
            onValueChange = { passwordStr = it },
            label = { Text("Password") },
            visualTransformation = if (passwordVisible) VisualTransformation.None else PasswordVisualTransformation(),
            trailingIcon = {
                val image = if (passwordVisible)
                    Icons.Default.Visibility
                else Icons.Default.VisibilityOff

                IconButton(onClick = { passwordVisible = !passwordVisible }) {
                    Icon(imageVector = image, "")
                }
            },
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
        )
        Spacer(modifier = Modifier.height(5.dp))

        OutlinedTextField(
            value = passwordStrConf,
            onValueChange = {
                Toast.makeText(MyApp.appContext, it, Toast.LENGTH_SHORT).show()
                passwordStrConf = it
            },
            label = { Text("Confirm Password") },
            visualTransformation = if (passwordVisible) VisualTransformation.None else PasswordVisualTransformation(),
            trailingIcon = {
                val image = if (passwordVisible)
                    Icons.Filled.Visibility
                else Icons.Default.VisibilityOff

                IconButton(onClick = { passwordVisible = !passwordVisible }) {
                    Icon(imageVector = image, "")
                }
            },
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
        )

        Spacer(modifier = Modifier.height(16.dp))

        Text(text = "Join our coven and accept our Terms & Conditions and Privacy Policy. Don't be afraid, we don't bite!")

        Spacer(modifier = Modifier.height(40.dp))

        val viewModel: UserViewModel = hiltViewModel()
        val userResponse by viewModel.userResponse.observeAsState()

        Button(
            onClick = { viewModel.addNewUser(user.apply { this.password = passwordStr }) },
            colors = ButtonDefaults.buttonColors(Color.Green),
            modifier = Modifier
                .fillMaxWidth()
                .height(48.dp)
        ) {
            Text(text = "Submit", color = Color.White)
        }

        when (val response = userResponse) {
            is ApiRespone.Error -> {
                Log.d(TAG, "PassWordScreen: ${response.message}")
                Toast.makeText(
                    LocalContext.current,
                    "Error: ${response.message} Status Code: ${response.statusCode}",
                    Toast.LENGTH_SHORT
                ).show()
            }

            ApiRespone.Loading -> {
                IndeterminateCircularProgress()
            }

            is ApiRespone.Success -> {
                Toast.makeText(LocalContext.current, response.data.message, Toast.LENGTH_SHORT)
                    .show()
                navController.navigate(Screens.Login.route)
            }

            null -> {
                Log.d(TAG, "Password Screen null tag")
            }
        }

        Spacer(modifier = Modifier.height(20.dp))

        val annotatedText = remember {
            AnnotatedString.Builder("Are you a familiar spirit ? again and join our Halloween party!")
                .apply {
                    addStringAnnotation(
                        tag = "JOIN_COVEN",
                        annotation = "Join Our Coven!",
                        start = 24,
                        end = 39
                    )
                }.toAnnotatedString()
        }
        ClickableText(
            text = annotatedText,
            onClick = { offset ->
                annotatedText.getStringAnnotations(
                    tag = "JOIN_COVEN",
                    start = offset,
                    end = offset
                )
                    .firstOrNull()?.let {
                        // Handle the click event
                    }
            },
            style = LocalTextStyle.current.copy(
                color = Color.Gray,
                fontSize = 14.sp,
                textAlign = TextAlign.Center
            )
        )
    }
}

@Composable
@Preview(showBackground = true)
fun PassWordScreenPreview() {
    PassWordScreen(
        User(1, "Praveen Kumar", "", "123456",
            "ADMIN","","ADMIN","Praveen","praveen.akash4@gmail.com"),
        rememberNavController()
    )
}