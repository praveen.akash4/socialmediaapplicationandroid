package com.praveen.socialmediaapplication.composables

import android.app.Activity
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.praveen.socialmediaapplication.R
import com.praveen.socialmediaapplication.viewmodel.FeedViewModel

@Composable
fun FeedScreen(navController: NavController, viewModel: FeedViewModel = hiltViewModel()) {
    val feedItems by viewModel.feedItems.collectAsState()
    val statusItems by viewModel.statusItems.collectAsState()

    val context = LocalContext.current

    BackHandler {
        // Custom behavior
        (context as? Activity)?.finish()
    }

    Column(
        modifier = Modifier
            .fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {

        ConstraintLayout(
            modifier = Modifier
                .fillMaxWidth()
                .padding(5.dp)
        ) {

            // Create references for the composables to constrain
            val (text, row) = createRefs()

            Spacer(modifier = Modifier.width(10.dp))

            Text("Social Media Application", style = TextStyle(
                fontStyle = FontStyle.Normal,
                fontWeight = FontWeight.Bold,
            ), fontSize = 16.sp,
                modifier = Modifier
                    .padding(15.dp, 0.dp, 0.dp, 0.dp)
                    .constrainAs(text) {
                        start.linkTo(parent.start)
                        top.linkTo(parent.top)
                        bottom.linkTo(parent.bottom)
                    })

            Row(modifier = Modifier
                .wrapContentWidth()
                .constrainAs(row) {
                    top.linkTo(parent.top)
                    end.linkTo(parent.end)
                }) {
                IconButton(onClick = { /* Handle Like Action */ }) {
                    Icon(
                        painter = painterResource(id = R.drawable.like),
                        contentDescription = "Like"
                    )
                }
                IconButton(onClick = { /* Handle Comment Action */ }) {
                    Icon(
                        painter = painterResource(id = R.drawable.share),
                        contentDescription = "Comment"
                    )
                }
            }
        }

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            // Profile Image
            Image(
                painter = painterResource(id = R.drawable.ic_launcher_background),
                // Ensure this drawable resource exists
                contentDescription = "Profile Picture",
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .size(80.dp)
                    .clip(CircleShape)
            )

            LazyRow {
                items(statusItems) { statusItems ->
                    LoadImageFromWeb(statusItems)
                }
            }
            Spacer(modifier = Modifier.width(1.dp))
        }

        LazyColumn {
            items(feedItems) { feedItem ->
                InstagramPostCard(feedItem = feedItem)
            }
        }
    }
}

