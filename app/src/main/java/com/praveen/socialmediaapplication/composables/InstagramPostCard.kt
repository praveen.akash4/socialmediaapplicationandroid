package com.praveen.socialmediaapplication.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.material3.Card
import androidx.compose.material3.CardColors
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.praveen.socialmediaapplication.data.FeedItem
import com.praveen.socialmediaapplication.R

@Composable
fun InstagramPostCard(feedItem: FeedItem) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(0.dp, 5.dp, 0.dp, 5.dp),
        elevation = CardDefaults.cardElevation(), colors = CardDefaults.cardColors(Color.White)
    ) {
        Column {
            // User Info
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.padding(8.dp)
            ) {
                Image(
                    painter = painterResource(feedItem.userImage),
                    contentDescription = "User Image",
                    modifier = Modifier
                        .size(35.dp)
                        .clip(CircleShape)
                        .padding(2.dp),
                    contentScale = ContentScale.Crop
                )
                Spacer(modifier = Modifier.width(8.dp))
                Text(
                    text = feedItem.userName,
                    fontWeight = FontWeight.Bold,
                    fontSize = 16.sp
                )
            }
            // Post Image
            AsyncImage(
                model = feedItem.imageUrl,
                contentDescription = "Post Image",
                modifier = Modifier
                    .fillMaxWidth()
                    .height(400.dp),
                contentScale = ContentScale.Crop
            )
            // Post Actions
            Row(
                modifier = Modifier.padding(5.dp, 0.dp, 0.dp, 0.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                IconButton(onClick = { /* Handle Like Action */ }) {
                    Icon(
                        painter = painterResource(id = R.drawable.like),
                        contentDescription = "Like"
                    )
                }
                IconButton(onClick = { /* Handle Comment Action */ }) {
                    Icon(
                        painter = painterResource(id = R.drawable.comment),
                        contentDescription = "Comment"
                    )
                }
            }

            // Post Likes and Comments
            Row(
                modifier = Modifier.padding(15.dp, 0.dp, 0.dp, 0.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = "${feedItem.likes} likes",
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier.padding(end = 8.dp)
                )
                Text(
                    text = "${feedItem.comments} comments"
                )
            }
            // Post Description
            Text(
                text = feedItem.description,
                modifier = Modifier
                    .padding(horizontal = 15.dp)
                    .padding(top = 5.dp)
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun InstagramPostCardPreview() {
    val feed =
        FeedItem("1", "Praveen Kumar", R.drawable.google, "Hello", "Happy Birthday", " ", 20, 30)
    InstagramPostCard(feedItem = feed)
}
