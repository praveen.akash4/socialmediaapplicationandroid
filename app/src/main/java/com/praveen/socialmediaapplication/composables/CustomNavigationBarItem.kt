package com.praveen.socialmediaapplication.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.unit.dp
import com.praveen.socialmediaapplication.bottomnavigation.BottomNavigationItem

@Composable
fun CustomNavigationBarItem(
    screen: BottomNavigationItem,
    isSelected: Boolean,
    onClick: () -> Unit
) {
    val background =
        if (isSelected) MaterialTheme.colorScheme.secondary else MaterialTheme.colorScheme.onBackground
    val contentColor =
        if (isSelected) MaterialTheme.colorScheme.onSecondary else MaterialTheme.colorScheme.onPrimary

    Column(
        modifier = Modifier
            .fillMaxHeight()
            .clip(MaterialTheme.shapes.small)
            .background(Color.Transparent)
            .clickable(onClick = onClick)
            .padding(vertical = 2.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Icon(
            painter = rememberVectorPainter(image = screen.icon),
            contentDescription = screen.route,
            tint = Color.Black,
            modifier = Modifier.size(30.dp)
        )
        Text(
            text = " ",
            color = contentColor,
            style = MaterialTheme.typography.labelSmall
        )
    }
}