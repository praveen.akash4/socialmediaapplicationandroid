package com.praveen.socialmediaapplication.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.praveen.socialmediaapplication.ApiRespone
import com.praveen.socialmediaapplication.data.User
import com.praveen.socialmediaapplication.data.UserResponse
import com.praveen.socialmediaapplication.repo.ApiRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UserViewModel @Inject constructor(private val apiRepository: ApiRepository) : ViewModel() {

    private var _userResponse = MutableLiveData<ApiRespone<UserResponse>>()
    public val userResponse : LiveData<ApiRespone<UserResponse>> get() = _userResponse

    fun addNewUser(user: User) {
        _userResponse.postValue(ApiRespone.Loading)

        viewModelScope.launch {
            apiRepository.addUser(user)?.catch {
                _userResponse.postValue(ApiRespone.Error(999, it.localizedMessage))
            }?.collect {
                _userResponse.postValue(it)
            }
        }
    }
}