package com.praveen.socialmediaapplication.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.praveen.socialmediaapplication.ApiRespone
import com.praveen.socialmediaapplication.repo.ApiRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FollowRequestViewModel @Inject constructor(private val repository: ApiRepository) :
    ViewModel() {

    private var _followStatus = MutableLiveData<ApiRespone<String>>()
    val followStatus: LiveData<ApiRespone<String>> get() = _followStatus

    fun sendFollowRequest(fromUserName: String, toUserName: String) {
        _followStatus.postValue(ApiRespone.Loading)
        viewModelScope.launch {
            repository.sendFollowRequest(fromUserName, toUserName).catch {
                _followStatus.postValue(ApiRespone.Error(999, it.localizedMessage))
            }.collect {
                _followStatus.postValue(it)
            }
        }
    }
}