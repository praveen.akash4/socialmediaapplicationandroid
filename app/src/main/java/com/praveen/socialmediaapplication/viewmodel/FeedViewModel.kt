package com.praveen.socialmediaapplication.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.praveen.socialmediaapplication.R
import com.praveen.socialmediaapplication.data.FeedItem
import com.praveen.socialmediaapplication.data.StatusItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FeedViewModel @Inject constructor() : ViewModel() {
    private val _feedItems = MutableStateFlow<List<FeedItem>>(emptyList())
    val feedItems: StateFlow<List<FeedItem>> = _feedItems

    private val _statusItems = MutableStateFlow<List<StatusItem>>(emptyList())
    val statusItems: StateFlow<List<StatusItem>> = _statusItems

    init {
        fetchFeedItems()
        fetchStatusItems()
    }

    private fun fetchFeedItems() {
        // Simulate fetching data
        viewModelScope.launch {
            val items = listOf(

                FeedItem("1", "Title 1", R.drawable.google,"Description 1","Hello", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT1aMNv1yD6kdacpVdHjWO5U56ZhYZiiWjWTw&s",10,10),
                FeedItem("2", "Title 2", R.drawable.google,"Description 2","Hello", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT1aMNv1yD6kdacpVdHjWO5U56ZhYZiiWjWTw&s",10,10),
                FeedItem("3", "Title 3", R.drawable.google,"Description 3","Hello", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQMP_3rw52GuUS-jtHkmIwQW1rIYzP41DgPWA&s",10,10)

            )
            _feedItems.value = items
        }
    }

    private fun fetchStatusItems() {
        // Simulate fetching data
        viewModelScope.launch {
            val items = listOf(

                StatusItem("1", "Title 1", R.drawable.google,"Description 1","Hello", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT1aMNv1yD6kdacpVdHjWO5U56ZhYZiiWjWTw&s"),
                StatusItem("2", "Title 2", R.drawable.google,"Description 2","Hello", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQMP_3rw52GuUS-jtHkmIwQW1rIYzP41DgPWA&s"),
                StatusItem("3", "Title 3", R.drawable.google,"Description 3","Hello", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQMP_3rw52GuUS-jtHkmIwQW1rIYzP41DgPWA&s")

            )
            _statusItems.value = items
        }
    }
}
