package com.praveen.socialmediaapplication.viewmodel

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.praveen.socialmediaapplication.ApiRespone
import com.praveen.socialmediaapplication.data.FeedItem
import com.praveen.socialmediaapplication.data.LoginResponse
import com.praveen.socialmediaapplication.data.User
import com.praveen.socialmediaapplication.repo.ApiRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchBarViewModel @Inject constructor(private val repository: ApiRepository) : ViewModel() {

    private val _text = MutableLiveData("")
    val text: LiveData<String> get() = _text

    private val _userItems = MutableLiveData<ApiRespone<List<User>>>()
    val userItems: LiveData<ApiRespone<List<User>>> get() = _userItems

    fun fetchData(query: String) {
        viewModelScope.launch {
            _userItems.postValue(ApiRespone.Loading)
            val response = repository.getUsers(query)
            response.catch {
                _userItems.postValue(ApiRespone.Error(999, it.localizedMessage))
            }.collect {
                _userItems.postValue(it)
            }
        }
    }
}