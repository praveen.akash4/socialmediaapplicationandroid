package com.praveen.socialmediaapplication.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.praveen.socialmediaapplication.ApiRespone
import com.praveen.socialmediaapplication.apiservice.APIService
import com.praveen.socialmediaapplication.data.Login
import com.praveen.socialmediaapplication.data.LoginResponse
import com.praveen.socialmediaapplication.repo.ApiRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AuthViewModel @Inject constructor(private val repository: ApiRepository) : ViewModel() {
    private val auth: FirebaseAuth by lazy { FirebaseAuth.getInstance() }
    private val _user = MutableStateFlow(auth.currentUser)
    val user: StateFlow<FirebaseUser?> = _user

    private val _loginResponse = MutableLiveData<ApiRespone<LoginResponse>>()
    val loginResponse : LiveData<ApiRespone<LoginResponse>> get() = _loginResponse

    fun signInWithGoogle(idToken: String) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        viewModelScope.launch {
            auth.signInWithCredential(credential).addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    _user.value = auth.currentUser
                } else {
                    _user.value = null
                }
            }
        }
    }

    fun login(login: Login) {
        _loginResponse.postValue(ApiRespone.Loading)

        viewModelScope.launch {
            repository.loginApi(login).catch {
                _loginResponse.postValue(ApiRespone.Error(999, it.message))
            }.collect {
                _loginResponse.postValue(it)
            }
        }
    }
}
