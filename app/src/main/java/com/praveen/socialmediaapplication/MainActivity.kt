package com.praveen.socialmediaapplication

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.lifecycle.ViewModelStore
import androidx.navigation.compose.rememberNavController
import com.praveen.socialmediaapplication.composables.AppNavigation
import com.praveen.socialmediaapplication.ui.theme.SocialmediaapplicationTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            SocialmediaapplicationTheme {
                AppNavigation()
            }
        }
    }
}